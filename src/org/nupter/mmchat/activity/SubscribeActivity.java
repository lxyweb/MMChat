package org.nupter.mmchat.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import com.avos.avoscloud.ParseInstallation;
import com.avos.avoscloud.PushService;
import org.nupter.mmchat.PrefsSaveHelper;
import org.nupter.mmchat.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class SubscribeActivity extends Activity {

    EditText editText;
    Button SubscribeButton;
    Button UnSubscribeButton;
    ListView listView;
    PrefsSaveHelper prefsSaveHelper;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);

        editText = (EditText) findViewById(R.id.editText);
        UnSubscribeButton = (Button) findViewById(R.id.unSubscribeButton);
        SubscribeButton = (Button) findViewById(R.id.subscribeButton);
        listView = (ListView)findViewById(R.id.listView);

        prefsSaveHelper = new PrefsSaveHelper(this);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(  this,
                android.R.layout.simple_list_item_1,
                getData()
        );

        listView.setAdapter(adapter);
        PushService.subscribe(SubscribeActivity.this,"123", MainActivity.class);
        PushService.subscribe(SubscribeActivity.this,"rabbit", MainActivity.class);
        ParseInstallation.getCurrentInstallation().saveInBackground();



        SubscribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PushService.subscribe(SubscribeActivity.this, editText.getText().toString(), MainActivity.class);
                Set<String> sets = prefsSaveHelper.getSubSet();
                if (sets == null) sets = new HashSet<String>();
                sets.add( editText.getText().toString());
                prefsSaveHelper.saveSubSet(sets);
                listView.notify();

            }
        });

        UnSubscribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PushService.unsubscribe(SubscribeActivity.this, editText.getText().toString());
                ParseInstallation.getCurrentInstallation().saveInBackground();


                Set<String> sets = prefsSaveHelper.getSubSet();
                if (sets == null) sets = new HashSet<String>();
                sets.remove( editText.getText().toString());
                prefsSaveHelper.saveSubSet(sets);
                listView.notify();

            }
        });



    }




    private ArrayList<String> getData(){
        ArrayList<String> list = new ArrayList<String>();

        Set<String> sets = prefsSaveHelper.getSubSet();
        if (sets != null) {
            for (String s : sets){
                list.add(s);
            }
        }else {
            list.add("你还没有订阅任何频道");
        }


        return list;
    }
}