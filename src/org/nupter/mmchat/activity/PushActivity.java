package org.nupter.mmchat.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.avos.avoscloud.ParseException;
import com.avos.avoscloud.ParsePush;
import com.avos.avoscloud.SendCallback;
import org.json.JSONObject;
import org.nupter.mmchat.R;


public class PushActivity extends Activity {

    Button sendChanalButton, sendAllButton, sendUserButton;
    EditText messageEdit, userEdit, ChannelEdit;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_push);

        messageEdit = (EditText)findViewById(R.id.editMessage);
        userEdit = (EditText)findViewById(R.id.editUser);
        ChannelEdit = (EditText)findViewById(R.id.editSubscirbtion);

        sendAllButton = (Button)findViewById(R.id.sendAllButton);
        sendUserButton = (Button)findViewById(R.id.sendUserButton);
        sendChanalButton = (Button)findViewById(R.id.sendCannelButton);

        sendAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParsePush push = new ParsePush();
                JSONObject object = new JSONObject();
                String message = messageEdit.getText().toString();
                try {
                    object.put("alert", message);
                }catch (Exception e){
                    e.printStackTrace();
                }
                push.setPushToAndroid(true);
                push.setData(object);
                push.sendInBackground(new SendCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            Toast.makeText(PushActivity.this, "推送成功", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(PushActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }





}
