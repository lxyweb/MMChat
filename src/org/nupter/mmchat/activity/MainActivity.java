package org.nupter.mmchat.activity;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import org.nupter.mmchat.R;

import java.util.ArrayList;

public class MainActivity extends ListActivity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView mListView = getListView();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(  this,
                android.R.layout.simple_list_item_1,
                getData()
        );

        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View v, int index, long arg3) {
                Intent intent = new Intent(MainActivity.this, activities[index].demoClass);
                MainActivity.this.startActivity(intent);
            }
        });
    }

    private static final ActivityInfo[] activities = {
            new ActivityInfo("Main", MainActivity.class),
            new ActivityInfo("Subscribe", SubscribeActivity.class),
            new ActivityInfo("register", RegisterActivity.class),
            new ActivityInfo("Push", PushActivity.class)



    };


    private ArrayList<String> getData(){
        ArrayList<String> list = new ArrayList<String>();
        for(ActivityInfo activity : activities){
            list.add(activity.title);
        }
        return list;
    }


    private static class ActivityInfo {
        private final String title;
        private final Class<? extends android.app.Activity> demoClass;

        public ActivityInfo(String title, Class<? extends android.app.Activity> demoClass) {
            this.title = title;
            this.demoClass = demoClass;
        }
    }



}
