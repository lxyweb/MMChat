package org.nupter.mmchat;

import android.app.Application;
import com.avos.avoscloud.Parse;
import com.avos.avoscloud.ParseInstallation;
import com.avos.avoscloud.ParsePush;
import com.avos.avoscloud.PushService;
import org.nupter.mmchat.activity.MainActivity;


public class MyApplication  extends Application {

    private static MyApplication instance;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;


        Parse.useAVCloudCN();
        Parse.initialize(this, "ty3gmgv4wy39ce0fip38179tr785kwzdxi36s0tjp6c1qlql", "frul1auqu2qpqayn4bvlfnj8if9cboqdwxzfv9tko6z4rzwk");
        PushService.setDefaultPushCallback(this, MainActivity.class);
        ParseInstallation.getCurrentInstallation().saveInBackground();



    }

    public static MyApplication getInstance() {
        /**
         *  静态方法获取Application 实例
         *  @see <a href="http://stackoverflow.com/questions/2002288/static-way-to-get-context-on-android">这个StackOverFlow问答</a>
         */
        return instance;
    }




}
