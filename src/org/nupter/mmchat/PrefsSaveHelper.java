package org.nupter.mmchat;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;


public class PrefsSaveHelper {


    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    Context context;

    public PrefsSaveHelper(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences("sub", Context.MODE_PRIVATE);

    }


    public void saveSubSet(Set<String> subscribeSet) {
        editor = prefs.edit();
        editor.putStringSet("sub", subscribeSet);
        editor.commit();
    }

    public Set<String> getSubSet(){
        return  prefs.getStringSet("sub", null);
    }
}
